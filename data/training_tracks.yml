- name: Git Newbies
  desc: | 
    Are you new to Git or version control in general? start here to learn about the basics that GitLab is based on 
    - source control, version management, git and the surrounding concepts.
  image: /images/events/getting-started-with-git.png
  tag: git
  items:
    - desc: Git-ing started with Git
      url: https://www.youtube.com/watch?v=Ce5nz5n41z4
    - desc: GitLab Flow
      url: https://www.youtube.com/watch?v=InKNIvky2KE
    - desc: 'Example: Updating the GitLab website with Git'
      url: https://www.youtube.com/watch?v=v6QbZMUpF28

- name: New to GitLab?
  desc: Learn how GitLab can help your team create value faster with a single application for the whole software development and operations lifecycle.
  image: /images/icons/about_us.png
  tag: gitlab
  items:
    - desc: GitLab flow
      url: https://www.youtube.com/watch?v=InKNIvky2KE
    - desc: 'Example: Updating the GitLab website with Git'
      url: https://www.youtube.com/watch?v=v6QbZMUpF28
    - desc: GitLab namespaces/groups
      url: https://www.youtube.com/watch?v=r0sJgjR2f5A
    - desc: Generating and using an SSH Key
      url: https://www.youtube.com/watch?v=54mxyLo3Mqk
    - desc: GitLab Web IDE
      url: https://www.youtube.com/watch?v=Y2SsnCHJd2w
    - desc: GitLab markdown tutorial
      url: https://about.gitlab.com/2018/08/17/gitlab-markdown-tutorial/
    - desc: GitLab Pages
      url: https://www.youtube.com/watch?v=TWqh9MtT4Bg

- name: Learn about GitLab CI/CD
  desc: GitLab CI/CD is part of GitLab. Learn how to use the industry leading CI/CD product to make your teams more productive.
  image: /images/icons/development.png
  tag: ci-cd
  items:
    - desc: Getting started with GitLab CI
      url: https://about.gitlab.com/2018/04/24/getting-started-gitlab-ci-gcp/
    - desc: GitLab CI/CD Demonstration
      url: https://about.gitlab.com/2017/03/13/ci-cd-demo/
    - desc: How we use GitLab CI/CD in our frontend team
      url: https://about.gitlab.com/2018/08/09/how-devops-and-gitlab-cicd-enhance-a-frontend-workflow/
    - desc: GitLab CI/CD and Google Cloud Platform
      url: https://www.youtube.com/watch?v=uWC2QKv15mk
    - desc: GitLab CI/CD and IBM Cloud Deploy
      url: https://www.youtube.com/watch?v=6ZF4vgKMd-g
    - desc: Test all the things!
      url: https://about.gitlab.com/2018/02/05/test-all-the-things-gitlab-ci-docker-examples/

- name: Concurrent DevOps
  desc: From project planning and source code management to CI/CD and monitoring, GitLab is a single application for the entire DevOps lifecycle.
  image: /images/icons/contributing.png
  tag: devops
  items:
    - desc: Overcoming barriers to DevOps (4 parts)
      url: https://www.youtube.com/watch?v=6vng3Sj-5As&list=PLFGfElNsQthap5pdPDBaxFPxYtnLyYoi4
    - desc: Avoiding the DevOps tax
      url: https://www.youtube.com/watch?v=iIElDMEC3U0
    - desc: Traditional DevOps daisy chain
      url: https://www.youtube.com/watch?v=YHznYB275Mg
    - desc: Auto DevOps Demonstration
      url: https://www.youtube.com/watch?v=4Uo_QP9rSGM
    - desc: Auto DevOps in action
      url: https://about.gitlab.com/2018/08/10/gitlab-auto-devops-in-action/

- name: Security
  desc: Dev, QA, Security and Operations are part of a single conversation throughout the lifecycle. Detect problems earlier by ‘shifting them to the left’ and solving them with out delays.
  tag: security
  items:
    - desc: Using GitLab for security
      url: https://www.youtube.com/watch?v=SP0VSH-NqJs
    - desc: What we learned at Google Next 2018
      url: https://about.gitlab.com/2018/08/10/google-next-2018-security-track-recap/

- name: GitLab Administration
  desc: Learn how to administer your GitLab instance to make sure it is highly available and useful for your teams.
  tag: admin
  items:
    - desc: GitLab High Availablity
      url: https://about.gitlab.com/high-availability/
    - desc: Maintenance and support
      url: https://www.youtube.com/watch?v=X8jsj59b4vk
    - desc: 'Integration: JIRA/Jenkins'
      url: https://www.youtube.com/watch?v=Jn-_fyra7xQ
    - desc: 'Integration: IBM Cloud'
      url: https://www.youtube.com/watch?v=6ZF4vgKMd-g
    - desc: 'Integration: Google Cloud Platform'
      url: https://www.youtube.com/watch?v=uWC2QKv15mk
    - desc: 'Integration: Connect to your private cloud'
      url: https://www.youtube.com/watch?v=j6HRDquF0mQ
    - desc: 'Integration: OpenShift'
      url: https://www.youtube.com/watch?v=EwbhA53Jpp4

- name: Project & Portfolio Management
  desc: As multiple projects scale, with parallel value streams and efforts, the organization needs to adopt processes to manage and govern the portfolio of agile projects, both in flight and proposed.
  tag: project
  items:
    - desc: Project & Portfolio management demonstration
      url: https://www.youtube.com/watch?v=LB5zvfjIDi0
    - desc: 4 ways to use GitLab Issue Boards
      url: https://about.gitlab.com/2018/08/02/4-ways-to-use-gitlab-issue-boards/

- name: GitLab Culture
  desc: GitLab's six values are Collaboration, Results, Efficiency, Diversity, Iteration, and Transparency, and together they spell the CREDIT we give each other by assuming good intent. Our values are interlinked and work with one another to protect our culture.
  image: /images/icons/help.png
  tag: culture
  items:
    - desc: This is GitLab
      url: https://www.youtube.com/watch?v=Mkw1-Uc7V1k
    - desc: GitLab's values
      url: https://about.gitlab.com/handbook/values/
    - desc: How working at GitLab has changed my view on work and life
      url: https://about.gitlab.com/2018/03/15/working-at-gitlab-affects-my-life/
    - desc: GitLabbers share how to recognize burnout (and how to prevent it)
      url: https://about.gitlab.com/2018/03/08/preventing-burnout/
    - desc: GitLab & Buffer CEOs talk transparency at scale
      url: https://about.gitlab.com/2017/03/14/buffer-and-gitlab-ceos-talk-transparency/
    - desc: Contributing to GitLab
      url: https://about.gitlab.com/2018/08/13/join-the-gitlab-community/
    - desc: Culture Blog posts
      url: https://about.gitlab.com/blog/categories/culture/
