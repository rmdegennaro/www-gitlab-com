---
layout: markdown_page
title: "Marketing Programs"
---


## On this page
{:.no_toc}

- TOC
{:toc}

## Marketing Programs

Marketing Programs focuses on executing, measuring and scaling GitLab's marketing programs such as email campaigns, event promotions, event follow up, drip email nurture series, webinars, and content. Marketing programs also aim to integrate data, personas and content to ensure relevant communications are delivered to the right person at the right time.

## Responsibilities

**Agnes Oetama**
* *Webcasts*: project management, set up, promotion, and follow up of all virtual events
* *Ad-Hoc Emails*: coordination of copy, review, and set up of one-time emails (i.e. security alert emails, package/pricing changes)
* *Bi-weekly Newsletter*: coordinate with content team on topics and review of newsletter

**Jackie Gragnola**
* *Event Support*: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* *Gated Content*: implement tracking, review copy, create landing pages, and test flows for gated whitepapers, reports, etc.
* *Nurture Campaigns*: strategize and campaigns (email nurturing)

**JJ Cordz**
* Cleaning and uploading of lead lists post-event


*Each manager will also own process revamp (including issue template updates and email template design refresh) that falls within their area of resposibility.*

Order of assignment for execution tasks: 
1. Primary responsible MPM  
2. Secondary MPM  (if primary MPM is OOO or not available)
3. Marketing OPS (if both MPMs are OOO or not available)


## Requesting to "Gate" a Piece of Content

Below is an overview of the process for requesting to put a new piece of content (such as a whitepaper, guide, report, etc.) behind a "gate" aka form on the website.

❌ **A landing page with a form should never be created without the inclusion and testing by Marketing Programs and/or Marketing Ops.**

Please contact Jackie Gragnola @jgragnola if you have any questions.

1. **TEMPLATE:** Create a new issue  using the *[Gated-Content-Request-MPM template](https://gitlab.com/gitlab-com/marketing/general/blob/master/.gitlab/issue_templates/Gated-Content-Request-MPM.md)*
2. **NAME:** The name of the issue should be *Gate Resource: [official name of content]*
3. **WIP:** If the content is in planning state, include *WIP:* until the contents of the piece are determined.
4. **DETAILS:** Fill in relevant details at the top of the issue (requester, type, official content name, and a link to citation policy for analysts)
5. **ASSIGN:** This issue will be automatically assigned to Jackie, who will fill in due dates and alert the proper team members to the next steps needed
6. **WHEN APPROVED:** Jackie will action (i.e. create campaigns, finance tags, set up, test, etc.).

## Requesting Marketing Programs Support for a Field Event

Below is an overview of the process for requesting support for a conference, field event, or owned event. Please contact Jackie Gragnola @jgragnola if you have any questions.

### Overview

The high-level steps are:
1) FMM creates meta issue of event details
2) Budget is approved and WIP is removed from issue
3) Jackie creates MPM Issue with all action items

### Creating the Meta Issue of the Event
* When creating the event meta issue, it will automatically be assigned to Jackie with the "Marketing Programs" tag and "MPM - Radar" tag.
* This will allow the issue to surface in her board, and the MPM issue will be promptly created and next steps defined.
* "WIP" should be at the beginning of the issue until it is approved, at which point the removal of "WIP:" will indicate to Jackie to create the MPM Support Issue.

### Event Channel Types

*[See full campaign progressions here](https://about.gitlab.com/handbook/business-ops/#conference)*

* **Conference:** Any large event that we have paid to sponsor, have a booth/presence at, and are sending representatives from GitLab. 
  *  Note: this is considered an Offline Channel for bizible reporting because we do not host a registration page, and receive a list of booth visitors post-event. 
  *  Example: DevOps Enterprise Summit, New York City Technology Forum
* **Field Event:** An event that we have paid to participate in but do not own the registration or event hosting duties.
  *  Note: this is considered an Offline Channel for bizible reporting because we do not host a registration page, and receive a list of attendees post-event. 
  *  Hint! If we do not own the registration page for the event, but it is not a conference (i.e. a dinner or breakfast), it is likely a Field Event. Comment in the issue to Jackie if you have need help.
  *  Example: Lighthouse Roadshow (hosted by Rancher), All Day DevOps (virtual event hosted by )
* **Owned Event:** This is an event that we have created, own registration and arrange speaker/venue. 
  *  Note: this is considered an Online Channel for bizible reporting because we manage the registration through our website.
  *  Example: GitLab Day Atlanta, Gary Gruver Roadshow

## Requesting an Email  

Process to request an email can be found in the [Business OPS](https://about.gitlab.com/handbook/business-ops/#requesting-an-email) section of the handbook.   

Primary party responsible for various email types can be determined using the [table above](#responsibilities).   
